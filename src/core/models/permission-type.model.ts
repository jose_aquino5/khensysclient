export default class PermissionTypeModel {
  public id = 0;
  public description = "";

  constructor(entity?: Partial<PermissionTypeModel>) {
    for (const key in entity) {
      if (this.hasOwnProperty(key)) {
        (this as any)[key] = (entity as any)[key];
      }
    }
  }
}

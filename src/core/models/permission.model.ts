export default class PermissionModel {
  public id = 0;
  public employeeName = "";
  public employeeLastName = "";
  public permissionTypeId = null;
  public permissionDate = null;

  constructor(entity?: Partial<PermissionModel>) {
    for (const key in entity) {
      if (this.hasOwnProperty(key)) {
        (this as any)[key] = (entity as any)[key];
      }
    }
  }
}

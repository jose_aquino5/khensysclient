import axios, { AxiosStatic, AxiosRequestConfig } from "axios";

export class HttpClient {
  private baseURL: string;
  private axios: AxiosStatic = axios;

  constructor(baseURL: string) {
    this.baseURL = baseURL;
    this.axios.defaults.baseURL = baseURL;
  }

  public async get(
    endpoint: string,
    wrapRes?: boolean,
    requestConfig?: AxiosRequestConfig
  ): Promise<any> {
    const res = await this.axios.get(`/${endpoint}`, requestConfig);
    return wrapRes ? res : res.data;
  }
  public async getById(
    endpoint: string,
    id: string,
    wrapRes?: boolean,
    requestConfig?: AxiosRequestConfig
  ): Promise<any> {
    const res = await this.axios.get(`/${endpoint}/${id}`, requestConfig);
    return wrapRes ? res : res.data;
  }
  public async post(
    endpoint: string,
    data: any,
    wrapRes?: boolean,
    requestConfig?: AxiosRequestConfig
  ): Promise<any> {
    const res = await this.axios.post(`/${endpoint}`, data, requestConfig);
    return wrapRes ? res : res.data;
  }
  public async patch(
    endpoint: string,
    data: any,
    wrapRes?: boolean,
    requestConfig?: AxiosRequestConfig
  ): Promise<any> {
    const res = await this.axios.patch(`/${endpoint}`, data, requestConfig);
    return wrapRes ? res : res.data;
  }
  public async put(
    endpoint: string,
    data: any,
    wrapRes?: boolean,
    requestConfig?: AxiosRequestConfig
  ): Promise<any> {
    const res = await this.axios.put(`/${endpoint}`, data, requestConfig);
    return wrapRes ? res : res.data;
  }
  public async delete(
    endpoint: string,
    wrapRes?: boolean,
    requestConfig?: AxiosRequestConfig
  ): Promise<any> {
    const res = await this.axios.delete(`/${endpoint}`, requestConfig);
    return wrapRes ? res : res.data;
  }
  public form(endpoint: string, data: any, filenames: string[]) {
    const formData = new FormData();
    Object.keys(data).forEach((key, i) =>
      formData.append(key, data[key], filenames[i])
    );

    return new Promise((resolve, reject) => {
      fetch(`${this.baseURL}/${endpoint}`, {
        method: "POST",
        body: formData
      })
        .then(res => res.json())
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }
}

export default new HttpClient(process.env.VUE_APP_API_ROOT);

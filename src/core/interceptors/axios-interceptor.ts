import axios from "axios";
import Swal from "sweetalert2";
import { defaultSwalConfig } from "@/assets/data";

function catchError(err: any) {
  if (!err.response)
    return Swal.fire({
      ...defaultSwalConfig,
      title: "Error",
      text: "Network error, please try again later.",
      icon: "error",
      timer: 3000
    });
  Swal.fire({
    ...defaultSwalConfig,
    title: "Error",
    text:
      err.response.data.message ||
      err.response.data.error.message ||
      "Unknown error.",
    icon: "error",
    timer: 3000
  });
}

export default function setup() {
  axios.interceptors.response.use(
    response => response,
    err => {
      catchError(err);
      return Promise.reject(err);
    }
  );
}

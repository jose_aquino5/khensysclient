import PermissionModel from "@/core/models/permission.model";
import { Component, Vue } from "vue-property-decorator";
import httpClients from "@/core/api/http-client.api";
import Swal from "sweetalert2";
import { defaultSwalConfig } from "@/assets/data";

@Component({})
export default class Permissions extends Vue {
  isLoading = false;
  permissions: PermissionModel[] = [];
  tfields: any[] = [
    { key: "id", label: "Identifier" },
    { key: "employeeName", label: "Employee Name" },
    { key: "employeeLastName", label: "Employee Last Name" },
    { key: "permissionTypeId", label: "Permission Type" },
    { key: "permissionDate", label: "Permission Date" },
    "action"
  ];

  mounted() {
    this.refreshRecords();
  }

  refreshRecords() {
    this.isLoading = true;
    httpClients
      .get(`api/permission`)
      .then(response => {
        this.permissions = response;
      })
      .finally(() => {
        this.isLoading = false;
      });
  }

  erase(id: number) {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!"
    }).then(result => {
      if (result.isConfirmed) {
        httpClients.delete(`api/permission/${id}`).then(() => {
          Swal.fire({
            ...defaultSwalConfig,
            title: "Deleted",
            text: "Your record has been deleted."
          });
          this.refreshRecords();
        });
      }
    });
  }
}

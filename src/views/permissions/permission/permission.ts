import { Component, Vue } from "vue-property-decorator";
import httpClients from "@/core/api/http-client.api";
import PermissionModel from "@/core/models/permission.model";
import Swal from "sweetalert2";
import { defaultSwalConfig } from "@/assets/data";
import PermissionTypeModel from "@/core/models/permission-type.model";

@Component({})
export default class Permission extends Vue {
  isLoading = false;
  model: PermissionModel = new PermissionModel();
  options: Array<PermissionTypeModel> = [];

  mounted() {
    if (
      this.$route.params.id &&
      !isNaN(parseInt(this.$route.params.id)) &&
      parseInt(this.$route.params.id) > 0
    ) {
      this.isLoading = true;
      httpClients
        .getById("api/permission", this.$route.params.id)
        .then(response => {
          this.model = new PermissionModel(response);
        })
        .finally(() => {
          this.isLoading = false;
        });
    }
    this.getOptions();
  }

  getOptions() {
    this.isLoading = true;
    httpClients
      .get(`api/permissiontype`)
      .then(response => {
        this.options = response;
      })
      .finally(() => {
        this.isLoading = false;
      });
  }

  async onSubmit(evt: any) {
    evt.preventDefault();
    if (await this.$validator.validateAll()) {
      this.isLoading = true;
      if (this.model.id > 0)
        httpClients
          .put(`api/permission/${this.model.id}`, this.model)
          .then(() => {
            Swal.fire({
              ...defaultSwalConfig,
              title: "Edited",
              text: "Record edited successfully"
            });
            this.$router.push({ name: "Permissions" });
          })
          .finally(() => {
            this.isLoading = false;
          });
      else
        httpClients
          .post("api/permission", this.model)
          .then(() => {
            Swal.fire({
              ...defaultSwalConfig,
              title: "Added",
              text: "Record added successfully"
            });
            this.$router.push({ name: "Permissions" });
          })
          .finally(() => {
            this.isLoading = false;
          });
    }
  }
}

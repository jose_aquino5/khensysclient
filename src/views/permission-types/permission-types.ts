import { Component, Vue } from "vue-property-decorator";
import httpClients from "@/core/api/http-client.api";
import PermissionTypeModel from "@/core/models/permission-type.model";
import Swal from "sweetalert2";
import { defaultSwalConfig } from "@/assets/data";

@Component({})
export default class PermissionTypes extends Vue {
  isLoading = false;
  permissionTypes: PermissionTypeModel[] = [];
  tfields: any[] = [
    { key: "id", label: "Identifier" },
    { key: "description", label: "Description" },
    "action"
  ];

  mounted() {
    this.refreshRecords();
  }

  refreshRecords() {
    this.isLoading = true;
    httpClients
      .get(`api/permissiontype`)
      .then(response => {
        this.permissionTypes = response;
      })
      .finally(() => {
        this.isLoading = false;
      });
  }

  erase(id: number) {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!"
    }).then(result => {
      if (result.isConfirmed) {
        httpClients.delete(`api/permissiontype/${id}`).then(() => {
          Swal.fire({
            ...defaultSwalConfig,
            title: "Deleted",
            text: "Your record has been deleted."
          });
          this.refreshRecords();
        });
      }
    });
  }
}

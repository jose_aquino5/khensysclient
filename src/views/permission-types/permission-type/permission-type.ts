import { Component, Vue } from "vue-property-decorator";
import httpClients from "@/core/api/http-client.api";
import PermissionTypeModel from "@/core/models/permission-type.model";
import Swal from "sweetalert2";
import { defaultSwalConfig } from "@/assets/data";

@Component({})
export default class PermissionType extends Vue {
  isLoading = false;
  model: PermissionTypeModel = new PermissionTypeModel();

  mounted() {
    if (
      this.$route.params.id &&
      !isNaN(parseInt(this.$route.params.id)) &&
      parseInt(this.$route.params.id) > 0
    ) {
      this.isLoading = true;
      httpClients
        .getById("api/permissiontype", this.$route.params.id)
        .then(response => {
          this.model = new PermissionTypeModel(response);
        })
        .finally(() => {
          this.isLoading = false;
        });
    }
  }

  async onSubmit(evt: any) {
    evt.preventDefault();
    if (await this.$validator.validateAll()) {
      this.isLoading = true;
      if (this.model.id > 0)
        httpClients
          .put(`api/permissiontype/${this.model.id}`, this.model)
          .then(() => {
            Swal.fire({
              ...defaultSwalConfig,
              title: "Edited",
              text: "Record edited successfully"
            });
            this.$router.push({ name: "PermissionTypes" });
          })
          .finally(() => {
            this.isLoading = false;
          });
      else
        httpClients
          .post("api/permissiontype", this.model)
          .then(() => {
            Swal.fire({
              ...defaultSwalConfig,
              title: "Added",
              text: "Record added successfully"
            });
            this.$router.push({ name: "PermissionTypes" });
          })
          .finally(() => {
            this.isLoading = false;
          });
    }
  }
}

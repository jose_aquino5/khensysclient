import { SweetAlertOptions } from "sweetalert2";

export const defaultSwalConfig: Partial<SweetAlertOptions> = {
  icon: "success",
  position: "top-end",
  showConfirmButton: false,
  timer: 1000
};

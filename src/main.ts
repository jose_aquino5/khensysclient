import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import VeeValidate from "vee-validate";
import moment from "moment";
import axiosInterceptorsSetup from "./core/interceptors/axios-interceptor";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "font-awesome/css/font-awesome.css";

// Install BootstrapVue
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);

Vue.use(VeeValidate, { fieldsBagName: "veeFields" });

Vue.config.productionTip = false;

Vue.prototype.moment = moment;

axiosInterceptorsSetup();

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
